# frozen_string_literal: true

# Molecule managed

describe apt('https://downloads.plex.tv/repo/deb') do
  it { should exist }
  it { should be_enabled }
end

describe package('plexmediaserver') do
  it { should be_installed }
end

describe service('plexmediaserver') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

nginx_config = [
  'server {',
  '    listen 80;',
  '    listen [::]:80;',
  '    server_name plex;',
  '    # enforce https',
  '    return 301 https://$server_name:443$request_uri;',
  '}',
  '',
  'server {',
  '    listen 443 ssl http2;',
  '    server_name plex;',
  '    ssl_certificate /etc/nginx/ssl/plex.crt;',
  '    ssl_certificate_key /etc/nginx/ssl/plex.key;',
  '    ssl_dhparam /etc/nginx/ssl/dhparam.pem;',
  '',
  '    #ssl on;',
  '    ssl_protocols TLSv1.2;',
  '    ssl_prefer_server_ciphers on;',
  '    ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";',
  '    ssl_ecdh_curve secp384r1;',
  '',
  '    add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";',
  '',
  '    add_header X-Content-Type-Options nosniff;',
  '    add_header X-XSS-Protection "1; mode=block";',
  '    add_header X-Robots-Tag none;',
  '    add_header X-Download-Options noopen;',
  '    add_header X-Permitted-Cross-Domain-Policies none;',
  '',
  '    access_log /var/log/nginx/plex-access.log;',
  '    error_log /var/log/nginx/plex-error.log;',
  '',
  '    location / {',
  '      proxy_pass http://127.0.0.1:32400/;',
  '      proxy_set_header Host $host;',
  '      proxy_set_header X-Real-IP $remote_addr;',
  '      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;',
  '    }',
  '}'
].join("\n")
describe file('/etc/nginx/sites-enabled/plex.conf') do
    its('content') { should match(nginx_config) }
end